
## Simple email form maker

### You can process our existing email form or you can get sample form code from us.

Looking for email form making? Our email form service that allows you to process any form on your website without installing and configuring a php program script

#### Our features:

* A/B Testing
* Form Conversion
* Form Optimization
* Branch logic
* Payment integration
* Third party integration
* Push notifications
* Multiple language support
* Conditional logic
* Validation rules
* Server rules
* Custom reports

### Lot of email templates are exist and you can choose whichever you want

When a visitor fills out a [email forms](http://www.formlogix.com/Email-Form/What-Is-An-Email-Form.aspx) on your website, the form data is sent to our server which then sends you an email containing the submitted form data designed by our [email form maker](https://formtitan.com).

Happy email form making!